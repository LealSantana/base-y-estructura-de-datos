## ENTREGABLE NUMERO 1 SCRIPS

## construcción de una base de datos en SQL Server para la clínica “Buena Salud”.
## Usamos la base de datos brindada por el Instructor agregando las 3 tablar restantes

## CORRECCION DE BASE DE DATOS 
```
USE EmpresaDB
GO

CREATE TABLE dept (
  Dept_No int NOT NULL,
  DNombre varchar(50) NOT NULL  UNIQUE,
  Loc  varchar(50) DEFAULT NULL,
  PRIMARY KEY (Dept_No)
) 

INSERT INTO dept VALUES (10,'Anatomía','Tacna')
INSERT INTO dept VALUES(20,'BioquímicaN','San Martin')
INSERT INTO dept VALUES(30,'Embriología','Miraflores')
INSERT INTO dept VALUES(40,'Fisiología','Santa Clara')
INSERT INTO dept VALUES(50,'Histología','Peru')
INSERT INTO dept VALUES(60,'Microbiología','Lima')
INSERT INTO dept VALUES(70,'Cirugía','Lomas')
INSERT INTO dept VALUES(80,'Pediatría','Tomal Valle')
INSERT INTO dept VALUES(90,'Psiquiatría','Avelino')
INSERT INTO dept VALUES(100,'Emergencias','Chiclayo')
INSERT INTO dept VALUES(110,'Ginecologia','Huanuco')


CREATE TABLE emp (
  Emp_No int NOT NULL,
  Apellido varchar(50) NOT NULL  UNIQUE,
  Oficio varchar(50) DEFAULT NULL,
  Dir int DEFAULT NULL,
  Fecha_Alt datetime DEFAULT NULL,
  Salario decimal(9,2) DEFAULT NULL,
  Comision decimal(9,2) DEFAULT NULL,
  Dept_No int DEFAULT NULL,
  PRIMARY KEY (Emp_No),
  CONSTRAINT FK_Emp FOREIGN KEY (Dept_No) REFERENCES dept (Dept_No)
)

INSERT INTO emp VALUES (1000,'García','Limpieza',2698,'2012-08-11 18:20:03',1955.00,0.00,30)
INSERT INTO emp VALUES (1111,'Rodríguez ','Medico',1902,'2024-05-21 10:15:31',1822.00,0.00,20)
INSERT INTO emp VALUES (1200,'González','Cocinero',3788,'2018-02-02 02:02:03',1530.00,0.00,20)
INSERT INTO emp VALUES (1300,'Fernández','Medico',4698,'2006-05-02 04:52:34',1335.00,0.00,30)
INSERT INTO emp VALUES (1400,'López','Enfermera',1566,'2022-06-04 02:13:34',3804.00,0.00,20)
INSERT INTO emp VALUES (1500,'Martínez','Paramedico',2782,'2014-07-31 12:52:34',1590.00,0.00,10)
INSERT INTO emp VALUES (1600,'Sánchez ','Camillera',3839,'2001-12-10 00:20:11',2131.00,39000.00,20)
INSERT INTO emp VALUES (1700,'Gómez','Medico',2119,'1999-07-30 20:52:34',1390.00,0.00,20)
INSERT INTO emp VALUES (2222,'Hernández','Bionalista',2698,'2000-03-20 12:15:31',2050.00,39000.00,30)
INSERT INTO emp VALUES (3333,'Salas','Enfermera',489,'2017-09-10 10:30:10',1623.00,65000.00,30)
INSERT INTO emp VALUES (4444,'Jimenez','Camillera',5839,'1999-01-01 12:40:12',3844.00,0.00,20)
INSERT INTO emp VALUES (5555,'Marin','Paramedico',6698,'2005-05-15 00:01:23',2422.00,182000.00,30)
INSERT INTO emp VALUES (6666,'Lionel','Contador',8839,'1999-07-31 12:52:34',1234.00,0.00,30)
INSERT INTO emp VALUES (7777,'Rivera','Director',9839,'1998-07-31 12:52:34',3123.00,0.00,10)
INSERT INTO emp VALUES (8888,'Pérez','Administrador',1566,'2010-02-04 09:10:04',3900.00,0.00,20)
INSERT INTO emp VALUES (9999,'Lopez','Chofer',0,'2012-01-31 02:09:34',2200.00,0.00,10)

CREATE TABLE enfermo (
  Inscripcion int NOT NULL,
  Apellido varchar(50) NOT NULL  UNIQUE,
  Direccion varchar(50) DEFAULT NULL,
  Fecha_Nac varchar(50) DEFAULT NULL,
  S varchar(2) DEFAULT NULL,
  NSS int DEFAULT NULL
)

INSERT INTO enfermo VALUES (23412,'Gomez','Jr. San Martin 20','16-may-20','M',180862422)
INSERT INTO enfermo VALUES(12312,'Gutierres','Av. Leoncio Prado 50','21-may-60','F',384991452)
INSERT INTO enfermo VALUES(34322,'Oscar','Jr. Ramon Castillo 12','23-jun-98','F',331790059)
INSERT INTO enfermo VALUES(45435,'Dominguez','Jr. Libertador 71','01-ene-97','M',123654471)
INSERT INTO enfermo VALUES(51232,'Lopez','Av. Amarilis 11','18-jun-22','F',380010212)
INSERT INTO enfermo VALUES(21232,'Georgue','Jr. Huanuco 38','29-feb-99','M',440294390)
INSERT INTO enfermo VALUES(55044,'Potter','Jr. Constantinopla 2','16-sep-23','F',444969044)
INSERT INTO enfermo VALUES(12303,'Grhoman','Av. Arica 103','26-dic-24','M',100973231)
INSERT INTO enfermo VALUES(12402,'Jackson','Av. Angamos 3','10-jul-97','F',243341231)
INSERT INTO enfermo VALUES(73443,'Pedro','Jr. Argentina','05-oct-25','M',123235555)
```
<kbd>
  <img src="IMG/correccion.png" />
</kbd>

## CREACION DE LA BASE DE DATOS CLINICA 

```
SET DATETIMEFORMAT YMD
GO

IF DB_ID('BDclinica_buena_salud') IS NOT NULL
    DROP DATABASE BDclinica_buena_salud
GO

CREATE DATABASE BDclinica_buena_salud
GO

USE BDclinica_buena_salud
GO

CREATE TABLE dept (
  Dept_No int NOT NULL,
  DNombre varchar(50) NOT NULL  UNIQUE,
  Loc  varchar(50) DEFAULT NULL,
  PRIMARY KEY (Dept_No)
) 

INSERT INTO dept VALUES (10,'Anatomía','Tacna')
INSERT INTO dept VALUES(20,'BioquímicaN','San Martin')
INSERT INTO dept VALUES(30,'Embriología','Miraflores')
INSERT INTO dept VALUES(40,'Fisiología','Santa Clara')
INSERT INTO dept VALUES(50,'Histología','Peru')
INSERT INTO dept VALUES(60,'Microbiología','Lima')
INSERT INTO dept VALUES(70,'Cirugía','Lomas')
INSERT INTO dept VALUES(80,'Pediatría','Tomal Valle')
INSERT INTO dept VALUES(90,'Psiquiatría','Avelino')
INSERT INTO dept VALUES(100,'Emergencias','Chiclayo')
INSERT INTO dept VALUES(110,'Ginecologia','Huanuco')


CREATE TABLE emp (
  Emp_No int NOT NULL,
  Apellido varchar(50) NOT NULL  UNIQUE,
  Oficio varchar(50) DEFAULT NULL,
  Dir int DEFAULT NULL,
  Fecha_Alt datetime DEFAULT NULL,
  Salario decimal(9,2) DEFAULT NULL,
  Comision decimal(9,2) DEFAULT NULL,
  Dept_No int DEFAULT NULL,
  PRIMARY KEY (Emp_No),
  CONSTRAINT FK_Emp FOREIGN KEY (Dept_No) REFERENCES dept (Dept_No)
)

INSERT INTO emp VALUES (1000,'García','Limpieza',2698,'2012-08-11 18:20:03',1955.00,0.00,30)
INSERT INTO emp VALUES (1111,'Rodríguez ','Medico',1902,'2024-05-21 10:15:31',1822.00,0.00,20)
INSERT INTO emp VALUES (1200,'González','Cocinero',3788,'2018-02-02 02:02:03',1530.00,0.00,20)
INSERT INTO emp VALUES (1300,'Fernández','Medico',4698,'2006-05-02 04:52:34',1335.00,0.00,30)
INSERT INTO emp VALUES (1400,'López','Enfermera',1566,'2022-06-04 02:13:34',3804.00,0.00,20)
INSERT INTO emp VALUES (1500,'Martínez','Paramedico',2782,'2014-07-31 12:52:34',1590.00,0.00,10)
INSERT INTO emp VALUES (1600,'Sánchez ','Camillera',3839,'2001-12-10 00:20:11',2131.00,39000.00,20)
INSERT INTO emp VALUES (1700,'Gómez','Medico',2119,'1999-07-30 20:52:34',1390.00,0.00,20)
INSERT INTO emp VALUES (2222,'Hernández','Bionalista',2698,'2000-03-20 12:15:31',2050.00,39000.00,30)
INSERT INTO emp VALUES (3333,'Salas','Enfermera',489,'2017-09-10 10:30:10',1623.00,65000.00,30)
INSERT INTO emp VALUES (4444,'Jimenez','Camillera',5839,'1999-01-01 12:40:12',3844.00,0.00,20)
INSERT INTO emp VALUES (5555,'Marin','Paramedico',6698,'2005-05-15 00:01:23',2422.00,182000.00,30)
INSERT INTO emp VALUES (6666,'Lionel','Contador',8839,'1999-07-31 12:52:34',1234.00,0.00,30)
INSERT INTO emp VALUES (7777,'Rivera','Director',9839,'1998-07-31 12:52:34',3123.00,0.00,10)
INSERT INTO emp VALUES (8888,'Pérez','Administrador',1566,'2010-02-04 09:10:04',3900.00,0.00,20)
INSERT INTO emp VALUES (9999,'Lopez','Chofer',0,'2012-01-31 02:09:34',2200.00,0.00,10)

CREATE TABLE enfermo (
  Inscripcion int NOT NULL,
  Apellido varchar(50) NOT NULL  UNIQUE,
  Direccion varchar(50) DEFAULT NULL,
  Fecha_Nac varchar(50) DEFAULT NULL,
  S varchar(2) DEFAULT NULL,
  NSS int DEFAULT NULL
)

INSERT INTO enfermo VALUES (23412,'Gomez','Jr. San Martin 20','16-may-20','M',180862422)
INSERT INTO enfermo VALUES(12312,'Gutierres','Av. Leoncio Prado 50','21-may-60','F',384991452)
INSERT INTO enfermo VALUES(34322,'Oscar','Jr. Ramon Castillo 12','23-jun-98','F',331790059)
INSERT INTO enfermo VALUES(45435,'Dominguez','Jr. Libertador 71','01-ene-97','M',123654471)
INSERT INTO enfermo VALUES(51232,'Lopez','Av. Amarilis 11','18-jun-22','F',380010212)
INSERT INTO enfermo VALUES(21232,'Georgue','Jr. Huanuco 38','29-feb-99','M',440294390)
INSERT INTO enfermo VALUES(55044,'Potter','Jr. Constantinopla 2','16-sep-23','F',444969044)
INSERT INTO enfermo VALUES(12303,'Grhoman','Av. Arica 103','26-dic-24','M',100973231)
INSERT INTO enfermo VALUES(12402,'Jackson','Av. Angamos 3','10-jul-97','F',243341231)
INSERT INTO enfermo VALUES(73443,'Pedro','Jr. Argentina','05-oct-25','M',123235555)
```

<kbd>
  <img src="IMG/31.png" />
</kbd>

<kbd>
  <img src="IMG/32.png" />
</kbd>


> Procedimiento almacenado para insertar un nuevo dept en SQL Server
```
CREATE PROCEDURE InsertarDept
  @Dept_No int ,
  @DNombre varchar(50) ,
  @Loc  varchar(50)
  
AS
BEGIN
    INSERT INTO dept(Dept_No, DNombre, Loc)
    VALUES (@Dept_No, @DNombre, @Loc);
END;
GO
```

> Procedimiento almacenado para actualizar un dept en SQL Server

```
CREATE PROCEDURE Actualizardept
  @Dept_No int ,
  @DNombre varchar(50) 

AS
BEGIN
    UPDATE dept
    SET DNombre =  @DNombre
    WHERE Dept_No = @Dept_No;
END;
GO
```
> Procedimiento almacenado para eliminar un dept en SQL Server

```
CREATE PROCEDURE Eliminardept
      @Dept_No int 
AS
BEGIN
    DELETE FROM dept WHERE Dept_No = @Dept_No;
END;
GO
```

> Procedimiento almacenado para insertar un nuevo Emp en SQL Server

```

CREATE PROCEDURE InsertarEmp
  @Emp_No int ,
  @Apellido varchar(50) ,
  @Oficio varchar(50),
  @Dir int ,
  @Fecha_Alt datetime ,
  @Salario decimal(9,2) ,
  @Comision decimal(9,2),
  @Dept_No int 
  
AS
BEGIN
    INSERT INTO emp(Emp_No,Apellido, Oficio, Dir, Fecha_Alt, Salario, Comision, Dept_No)
    VALUES ( @Emp_No ,
  @Apellido  ,
  @Oficio ,
  @Dir  ,
  @Fecha_Alt  ,
  @Salario ,
  @Comision,
  @Dept_No)

END;
GO
```

>Procedimiento almacenado para actualizar un EMP en SQL Server

```
CREATE PROCEDURE ActualizarEmp
@Emp_No int ,
  @Apellido varchar(50) ,
  @Oficio varchar(50)

AS
BEGIN
    UPDATE emp
    SET Apellido =  @Apellido
    WHERE Emp_No = @Emp_No;
END;
GO
```

> Procedimiento almacenado para eliminar un dept en SQL Server

```
CREATE PROCEDURE EliminarEmp
     @Emp_No int 
AS
BEGIN
    DELETE FROM emp WHERE Emp_No = @Emp_No;
END;
GO
```

> Procedimiento almacenado para insertar un nuevo enfermo en SQL Server

```

CREATE PROCEDURE InsertarEnfermo
  @Inscripcion int ,
  @Apellido varchar(50) ,
  @Direccion varchar(50),
  @Fecha_Nac varchar(50) ,
  @S varchar(2),
  @NSS int 
AS
BEGIN
    INSERT INTO enfermo(Inscripcion, Apellido, Direccion, Fecha_Nac, S, NSS)
    VALUES (@Inscripcion, @Apellido, @Direccion,@Fecha_Nac, @S, @NSS);
END;
GO
```

> Procedimiento almacenado para actualizar un enfermo en SQL Server
```
CREATE PROCEDURE ActualizarEnfermo
 @Inscripcion int ,
  @Apellido varchar(50) ,
  @Direccion varchar(50)
    
AS
BEGIN
    UPDATE enfermo
    SET Apellido = @Apellido, Direccion = @Direccion
    WHERE Inscripcion = @Inscripcion;
END;
GO
```

> Procedimiento almacenado para eliminar un enfermo en SQL Server

```
CREATE PROCEDURE EliminarEnfermo
    @Inscripcion int 
AS
BEGIN
    DELETE FROM enfermo WHERE Inscripcion = @Inscripcion;
END;
GO
```
<kbd>
  <img src="IMG/isertado.png" />
</kbd>

## CONSULTAS 
## 1 obtener todos los empleados que se dieron de alta antes del año 2018 y que pertenecen a un determinado departamento. Además, utilizando los comandos DCL.

```
DECLARE @DeptNo INT = 20
SELECT *
FROM Emp
WHERE Fecha_Alt  <'2018-02-02'
 AND Dept_No = @DeptNo;

GO
```
<kbd>
  <img src="IMG/cons.png" />
</kbd>

## 2 Crear un procedimiento almacenado que permita insertar un nuevo departamento, crear un procedimiento que recupere el promedio de edad de las personas por cada departamento. 

```
CREATE PROCEDURE InsertarDept
  @Dept_No int ,
  @DNombre varchar(50) ,
  @Loc  varchar(50)
  
AS
BEGIN
    INSERT INTO dept(Dept_No, DNombre, Loc)
    VALUES (@Dept_No, @DNombre, @Loc);
END;
GO
```

## 3crear un procedimiento para devolver el apellido, oficio y salario, pasándole como parámetro el número del empleado. 
```
CREATE PROCEDURE sp_obtener_emp
    @Emp_No  int
AS
BEGIN
    SELECT Apellido, Oficio, Salario
    FROM emp
    WHERE Emp_No= @Emp_No;
END
EXEC sp_obtener_emp  @Emp_No = 1000; 
```

## 4 Crear un procedimiento almacenado para dar de baja a un empleado pasándole como parámetro su apellido. sql server
```
CREATE PROCEDURE sp_dar_de_baja_emp
    @Apellido varchar(50)
AS
BEGIN
    DELETE FROM emp
    WHERE Apellido = @Apellido;
END
EXEC sp_dar_de_baja_emp @Apellido = 'Lopez';
```
## 5 Crear restricciones al modelo de BD, para asegurar la calidad de la información.
```
CREATE LOGIN userEjemplo WITH PASSWORD = 'ContraseñaSegura123!';
CREATE USER userEjemplo FOR LOGIN userEjemplo;

GRANT SELECT ON emp TO userEjemplo;

GRANT INSERT ON Emp_No TO userjemplo;

GRANT UPDATE ON emp TO userEjemplo;
```
## 6  crea procedimientos almacenados para ingresar, consultar, modificar y eliminar datos de la base, junto con funciones.

> Procedimiento almacenado para insertar un nuevo dept en SQL Server

```
CREATE PROCEDURE InsertarDept
  @Dept_No int ,
  @DNombre varchar(50) ,
  @Loc  varchar(50)
  
AS
BEGIN
    INSERT INTO dept(Dept_No, DNombre, Loc)
    VALUES (@Dept_No, @DNombre, @Loc);
END;
GO
```

> Procedimiento almacenado para actualizar un dept en SQL Server

```
CREATE PROCEDURE Actualizardept
  @Dept_No int ,
  @DNombre varchar(50) 

AS
BEGIN
    UPDATE dept
    SET DNombre =  @DNombre
    WHERE Dept_No = @Dept_No;
END;
GO
```

> Procedimiento almacenado para eliminar un dept en SQL Server

```
CREATE PROCEDURE Eliminardept
      @Dept_No int 
AS
BEGIN
    DELETE FROM dept WHERE Dept_No = @Dept_No;
END;
GO
```

> Procedimiento almacenado para insertar un nuevo Emp en SQL Server

```
CREATE PROCEDURE InsertarEmp
  @Emp_No int ,
  @Apellido varchar(50) ,
  @Oficio varchar(50),
  @Dir int ,
  @Fecha_Alt datetime ,
  @Salario decimal(9,2) ,
  @Comision decimal(9,2),
  @Dept_No int 
  
  
AS
BEGIN
    INSERT INTO emp(Emp_No,Apellido, Oficio, Dir, Fecha_Alt, Salario, Comision, Dept_No)
    VALUES ( @Emp_No ,
  @Apellido  ,
  @Oficio ,
  @Dir  ,
  @Fecha_Alt  ,
  @Salario ,
  @Comision,
  @Dept_No)

END;
GO
```

> Procedimiento almacenado para actualizar un dept en SQL Server

```
CREATE PROCEDURE ActualizarEmp
@Emp_No int ,
  @Apellido varchar(50) ,
  @Oficio varchar(50)

AS
BEGIN
    UPDATE emp
    SET Apellido =  @Apellido
    WHERE Emp_No = @Emp_No;
END;
GO
```

> Procedimiento almacenado para eliminar un dept en SQL Server

```
CREATE PROCEDURE EliminarEmp
     @Emp_No int 
AS
BEGIN
    DELETE FROM emp WHERE Emp_No = @Emp_No;
END;
GO
```

> Procedimiento almacenado para insertar un nuevo enfermo en SQL Server

```
CREATE PROCEDURE InsertarEnfermo
  @Inscripcion int ,
  @Apellido varchar(50) ,
  @Direccion varchar(50),
  @Fecha_Nac varchar(50) ,
  @S varchar(2),
  @NSS int 
AS
BEGIN
    INSERT INTO enfermo(Inscripcion, Apellido, Direccion, Fecha_Nac, S, NSS)
    VALUES (@Inscripcion, @Apellido, @Direccion,@Fecha_Nac, @S, @NSS);
END;
GO
```

> Procedimiento almacenado para actualizar un enfermo en SQL Server

```
CREATE PROCEDURE ActualizarEnfermo
 @Inscripcion int ,
  @Apellido varchar(50) ,
  @Direccion varchar(50)
    
AS
BEGIN
    UPDATE enfermo
    SET Apellido = @Apellido, Direccion = @Direccion
    WHERE Inscripcion = @Inscripcion;
END;
GO
```

> Procedimiento almacenado para eliminar un enfermo en SQL Server

```
CREATE PROCEDURE EliminarEnfermo
    @Inscripcion int 
AS
BEGIN
    DELETE FROM enfermo WHERE Inscripcion = @Inscripcion;
END;
GO
```
