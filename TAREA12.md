## EJERCICIO PROPUESTOS 
**Ordenar de menor a mayor y dar formato en soles en todas las consulas que tienen un valor en ventas.**

## 1. ¿Cuál es el total de ventas realizadas en el año 2009?

```
DECLARE @fecha_inicio_2009 datetime = '2009-01-01';
DECLARE @fecha_fin_2009  datetime= '2009-12-31';
SELECT SUM(total) AS Total_Ventas, FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM ve.documento
WHERE fechaMovimiento BETWEEN @fecha_inicio_2009 AND @fecha_fin_2009;
```
<kbd>
  <img src="IMG/1.png" />
</kbd>

## 2. ¿Cuáles son las personas que no tienen una entrada registrada en la tabla personaDestino?

```
SELECT p.*
FROM ma.persona p
LEFT JOIN ma.personaDestino pd ON p.persona = pd.persona
WHERE pd.persona IS NULL
ORDER BY(pd.personaDestino);
```
<kbd>
  <img src="IMG/2.png" />
</kbd>

## 3. ¿Cuál es el promedio del monto total de todas las transacciones de ventas registradas en la base de datos, expresado en moneda local (soles peruanos)?
```
SELECT FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM ve.documento;
```
<kbd>
  <img src="IMG/3.png" />
</kbd>

## 4. Obtén una lista de todos los documentos de ventas cuyo monto total supere el promedio del monto total de todos los documentos de ventas registrados en la base de datos.

```
SELECT *
FROM ve.documento
WHERE total > (SELECT AVG(total) FROM ve.documento)
ORDER BY(documento);
```

<kbd>
  <img src="IMG/4.png" />
</kbd>

## 5. Listar los documentos de ventas que han sido pagados utilizando una forma de pago específica desde la tabla documentoPago.

```
SELECT d.*
FROM ve.documentoPago dp
INNER JOIN ve.documento d ON dp.documento = d.documento
INNER JOIN pa.pago p ON dp.pago = p.pago
WHERE p.formaPago = 1
ORDER BY (dp.documento),(p.pago),(p.formaPago);
```

<kbd>
  <img src="IMG/5.png" />
</kbd>

## 6. ¿Cómo se distribuye el saldo total entre los diferentes almacenes, considerando la información de los saldos iniciales de inventario en la base de datos?

```
SELECT almacen, FORMAT(AVG(costoSoles), 'C', 'es-PE') AS Saldo_Total
FROM ma.saldosIniciales
GROUP BY almacen
ORDER BY(almacen),(Saldo_Total);
```
<kbd>
  <img src="IMG/6.png" />
</kbd>

## 7. Obtén una lista de todos los documentos de ventas cuyo monto total supere el promedio del monto total de todos los documentos de ventas registrados en la base de datos.

```
SELECT *
FROM ve.documento
WHERE total > (SELECT AVG(total) FROM ve.documento)
ORDER BY(documento);
```
<kbd>
  <img src="IMG/7.png" />
</kbd>

## 8. ¿Cuáles son los detalles de los documentos de ventas asociados al vendedor con identificación número 3 en la base de datos,  considerando la información detallada de cada documento en relación con sus elementos de venta?

```
SELECT d.*
FROM ve.documento d
INNER JOIN ve.documentoDetalle dd ON d.documento = dd.documento
WHERE d.vendedor = 3
ORDER BY(dd.documento);
```
<kbd>
  <img src="IMG/8.png" />
</kbd>

## 9.¿Cuál es el total de ventas por año y vendedor en la base de datos de ventas,  considerando solo aquellos vendedores cuya suma total de ventas en un año específico sea superior a 100,000 unidades monetarias?

```
SELECT vendedor, YEAR(fechaMovimiento) AS Anio, AVG(total) AS Total_Ventas, FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM ve.documento
GROUP BY vendedor, YEAR(fechaMovimiento)
HAVING SUM(total) > 100000
ORDER BY (vendedor),(total_ventas);
```
<kbd>
  <img src="IMG/9.png" />
</kbd>

## 10. ¿Cuál es el desglose mensual de las ventas por vendedor en cada año, considerando la suma total de ventas para cada mes y año específico?

```
SELECT vendedor, MONTH (fechaMovimiento) AS Anio, AVG(total) AS Total_Ventas, FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM ve.documento
GROUP BY vendedor, MONTH (fechaMovimiento)
HAVING SUM(total) > 100000
ORDER BY (vendedor),(total_ventas) ;
```
<kbd>
  <img src="IMG/10.png" />
</kbd>

## 11. ¿Cuántos clientes compraron más de 10 veces en un año?

```
SELECT persona, YEAR(fechaMovimiento) AS Año, COUNT(*) AS Compras, FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM ve.documento
WHERE tipoMovimiento = 1
GROUP BY persona, YEAR(fechaMovimiento)
HAVING COUNT(*) > 10
ORDER BY (persona),( Año);
```
<kbd>
  <img src="IMG/11.png" />
</kbd>

## 12. ¿Cuál es el total acumulado de descuentos aplicados por cada vendedor en la base de datos de ventas, considerando la suma de los descuentos descto01, descto02 y descto03,  y mostrando solo aquellos vendedores cuyo total de descuentos acumulados supere los 5000?

```
SELECT vendedor, FORMAT(AVG(descto01 + descto02 + descto03),'C','es-PE') 
AS Descuentos_Acumulados, FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM ve.documento
GROUP BY vendedor
HAVING sum(descto01 + descto02 + descto03) > 5000
ORDER BY(vendedor),(Descuentos_Acumulados);
```
<kbd>
  <img src="IMG/12.png" />
</kbd>

## 13. ¿Cuál es el total anual de ventas realizadas por cada persona en la base de datos de ventas, considerando únicamente los movimientos de tipo venta (tipoMovimiento = 1), y mostrando solo aquellas personas cuyas ventas anuales superen los 10000?

```
SELECT persona, YEAR(fechaMovimiento) AS Año, FORMAT(AVG(total),'C','es-PE') 
AS Total_Anual, FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM ve.documento
WHERE tipoMovimiento = 1
GROUP BY persona, YEAR(fechaMovimiento)
HAVING SUM(total) > 10000
ORDER BY (persona),(Año);
```
<kbd>
  <img src="IMG/13.png" />
</kbd>

## 14. ¿Cuál es el recuento total de productos vendidos por cada vendedor en la base de datos de ventas?

```
SELECT 
    d.vendedor, 
    COUNT(dd.documentoDetalle) AS Total_Productos_Vendidos
FROM 
    ve.documentoDetalle dd
JOIN 
    ve.documento d ON dd.documento = d.documento
GROUP BY 
    d.vendedor
ORDER BY(vendedor);
```
<kbd>
  <img src="IMG/14.png" />
</kbd>

## 15. ¿Cuánto se vendió cada mes del año 2009, desglosado por tipo de pago?

```
SELECT 
    MONTH(d.fechaMovimiento) AS Mes,
    p.formaPago,
    SUM(d.total) AS Total_Ventas
FROM 
    ve.documento d
JOIN 
    pa.pago p ON d.vendedor = p.vendedor
WHERE 
    YEAR(d.fechaMovimiento) = 2009
GROUP BY 
    MONTH(d.fechaMovimiento),
    p.formaPago
ORDER BY(Mes),(formaPago),(Total_Ventas);
```
<kbd>
  <img src="IMG/15.png" />
</kbd>

## 16. Cuál es el total de ventas realizadas en el año 2007?

```
DECLARE @fecha_inicio_2007 datetime = '2007-01-01';
DECLARE @fecha_fin_2007  datetime= '2007-12-31';
SELECT SUM(total) AS Total_Ventas, FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM ve.documento
WHERE fechaMovimiento BETWEEN @fecha_inicio_2007 AND @fecha_fin_2007;
```
<kbd>
  <img src="IMG/16.png" />
</kbd>

## 7. ¿Cuáles son las personas que no tienen una entrada registrada en la tabla personaDestino en el año 2008?

```
SELECT p.*
FROM ma.persona p
LEFT JOIN ma.personaDestino pd ON p.persona = pd.persona
WHERE pd.persona IS NULL
ORDER BY(pd.personaDestino);
```
```
SELECT p.*
FROM ma.persona p
LEFT JOIN ma.personaDestino pd ON p.persona = pd.persona
WHERE pd.persona IS NULL
AND YEAR(pd.fecha) = 2008
ORDER BY p.persona;
```
<kbd>
  <img src="IMG/17.png" />
</kbd>

## 18. ¿Cuál es el promedio del monto total de todas las  transacciones de ventas registradas en la base de datos en el año 2009, expresado en moneda local (soles peruanos)?

```
SELECT FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM ve.documento
WHERE YEAR(fechaMovimiento) = 2009;
```
<kbd>
  <img src="IMG/18.png" />
</kbd>

## 19. Obtén una lista de todos los documentos de ventas cuyo monto total supere el promedio del monto total de todos los documentos de ventas registrados en la base de datos en el año 2005.

```
SELECT *
FROM ve.documento
WHERE total > (SELECT AVG(total) FROM ve.documento WHERE YEAR(fechaMovimiento) = 2005)
ORDER BY (documento),(tipoDocumento);
```
<kbd>
  <img src="IMG/19.png" />
</kbd>

## 20. Listar los documentos de ventas que han sido pagados utilizando una forma de pago específica desde la tabla documentoPago en el año 2006.

```
SELECT d.*
FROM ve.documentoPago dp
INNER JOIN ve.documento d ON dp.documento = d.documento
INNER JOIN pa.pago p ON dp.pago = p.pago
WHERE p.formaPago = 1 AND YEAR(fechaMovimiento) = 2006
ORDER BY (documento)
```
<kbd>
  <img src="IMG/20.png" />
</kbd>

## 21. ¿Cómo se distribuye el saldo total entre los diferentes almacenes, considerando la información de los saldos iniciales de inventario en la base de datos en el año 2007?

```
SELECT 
    almacen, 
    SUM(costoSoles) AS Saldo_Total
FROM 
    ma.saldosIniciales
WHERE 
    YEAR(periodo) = 2007
GROUP BY 
    almacen
	ORDER BY almacen;
```
<kbd>
  <img src="IMG/21.png" />
</kbd>

## 22. Obtén una lista de todos los documentos de ventas cuyo monto total supere el promedio del monto total de todos los documentos de ventas registrados en la base de datos en el año 2008.

```
SELECT *
FROM [ve].[documento]
WHERE total > (
    SELECT AVG(total)
    FROM [ve].[documento]
    WHERE YEAR(fechaMovimiento) = 2008
)
AND YEAR(fechaMovimiento) = 2008
ORDER BY documento
```
<kbd>
  <img src="IMG/22.png" />
</kbd>

## 23. ¿Cuáles son los detalles de los documentos de ventas asociados al vendedor con identificación número 3 en la base de datos en el año 2009, considerando la información detallada de cada documento en relación con sus elementos de venta?

```
SELECT d.*,
dd.*,
FORMAT (d.total, 'C','es-PE') AS Total_Ventas_En_Soles
FROM ve.documento d
INNER JOIN ve.documentoDetalle dd ON  d.documento = dd.documento
WHERE d.vendedor = 3 AND YEAR(d.fechaMovimiento) = 2009
ORDER BY d.total;
```
<kbd>
  <img src="IMG/23.png" />
</kbd>

## 24. ¿Cuál es el total de ventas por año y vendedor en la base de datos de ventas, considerando solo aquellos vendedores cuya suma total de ventas en el año 2008 sea superior a 100,000 unidades monetarias?

```
SELECT 
    vendedor, 
    YEAR(fechaMovimiento) AS Anio, 
    SUM(total) AS Total_Ventas, 
    FORMAT(SUM(total), 'C', 'es-PE') AS [Monto Total en Soles]
FROM 
    ve.documento
GROUP BY 
    vendedor, 
    YEAR(fechaMovimiento)
HAVING 
    YEAR(fechaMovimiento) = 2008 AND
    SUM(total) > 100000
ORDER BY 
    vendedor, Total_Ventas;
```
<kbd>
  <img src="IMG/24.png" />
</kbd>

## 25. ¿Cuál es el desglose mensual de las ventas por vendedor en cada año, considerando la suma total de ventas para cada mes y año específico en el año 2009?

```
SELECT 
    vendedor, 
    YEAR(fechaMovimiento) AS Anio, 
    MONTH(fechaMovimiento) AS Mes, 
    SUM(total) AS Total_Ventas, 
    FORMAT(SUM(total), 'C', 'es-PE') AS [Total de Ventas en Soles]FROM  ve.documento
WHERE YEAR(fechaMovimiento) = 2009
GROUP BY vendedor, 
    YEAR(fechaMovimiento), 
    MONTH(fechaMovimiento)
ORDER BY 
    vendedor, 
    YEAR(fechaMovimiento), 
    MONTH(fechaMovimiento);
```
<kbd>
  <img src="IMG/25.png" />
</kbd>

## 26. ¿Cuántos clientes compraron más de 10 veces en un año en el año 2005?

```
SELECT 
    persona, 
    YEAR(fechaMovimiento) AS Año, 
    COUNT(*) AS Compras, 
    FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM 
    ve.documento
WHERE 
    tipoMovimiento = 1 AND
    YEAR(fechaMovimiento) = 2005
GROUP BY 
    persona, 
    YEAR(fechaMovimiento)
HAVING 
    COUNT(*) > 10
ORDER BY 
    Año;
	--CERO**
```
<kbd>
  <img src="IMG/26.png" />
</kbd>

## 27. ¿Cuál es el total acumulado de descuentos aplicados por cada vendedor en la base de datos de ventas, considerando la suma de los descuentos descto01, descto02 y descto03, y mostrando solo aquellos vendedores cuyo total de descuentos acumulados supere los 5000 en el año 2005?

```
SELECT 
    vendedor, 
    SUM(descto01 + descto02 + descto03) AS Descuentos_Acumulados, 
    FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM 
    ve.documento
WHERE 
    YEAR(fechaMovimiento) = 2005
GROUP BY 
    vendedor
HAVING 
    SUM(descto01 + descto02 + descto03) > 5000
ORDER BY 
    vendedor;
```
<kbd>
  <img src="IMG/27.png" />
</kbd>

## 28. ¿Cuál es el total anual de ventas realizadas por cada persona en la base de datos de ventas, considerando únicamente los movimientos de tipo venta (tipoMovimiento = 1), y mostrando solo aquellas personas cuyas ventas anuales superen los 10000 en el año 2007?

```
SELECT persona, YEAR(fechaMovimiento) AS Año, FORMAT(SUM(total), 'C', 'es-PE') AS Total_Anual
FROM ve.documento
WHERE tipoMovimiento = 1
AND YEAR(fechaMovimiento) = 2007
GROUP BY persona, YEAR(fechaMovimiento)
HAVING SUM(total) > 10000
ORDER BY Año;
```
<kbd>
  <img src="IMG/28.png" />
</kbd>

## 29. ¿Cuál es el recuento total de productos vendidos por cada vendedor en la base de datos de ventas en el año 2008?

```
SELECT 
    d.vendedor, 
    COUNT(dd.documentoDetalle) AS Total_Productos_Vendidos
FROM 
    ve.documentoDetalle dd
JOIN 
    ve.documento d ON dd.documento = d.documento
WHERE 
    YEAR(d.fechaMovimiento) = 2008
GROUP BY 
    d.vendedor;
```
<kbd>
  <img src="IMG/29.png" />
</kbd>

## 30. ¿Cuánto se vendió cada mes del año 2009, desglosado por tipo de pago?

```
SELECT 
FORMAT(MONTH(d.fechaMovimiento), 'c','es-PE')AS Mes,
p.formaPago,
SUM(d.total) AS Total_Ventas
FROM 
ve.documento d
JOIN pa.pago p ON d.vendedor = p.vendedor
WHERE 
YEAR (d.fechaMovimiento) = 2009
GROUP BY 
MONTH (d.fechaMovimiento),
p.formaPago
ORDER BY (Mes)
```
<kbd>
  <img src="IMG/30.png" />
</kbd>

## CREACION DE LA BASE DE DATOS DEL LIBRO

```
SET DATEFORMAT YMD
GO

IF DB_ID('BDventas') IS NOT NULL
    DROP DATABASE BDventas
GO

CREATE DATABASE BDventas
GO

USE BDVentas
GO

CREATE TABLE DISTRITO(
    COD_DIS CHAR(5) NOT NULL PRIMARY KEY,
    NOM_DIS VARCHAR(50) NOT NULL
)
GO

CREATE TABLE VENDEDOR(
    COD_VEN CHAR(3) NOT NULL PRIMARY KEY,
    NOM_VEN VARCHAR(20) NOT NULL,
    APE_VEN VARCHAR(20) NOT NULL,
    SUE_VEN MONEY NOT NULL,
    FTI_VEN DATE NOT NULL,
    TIP_VEN VARCHAR(10) NOT NULL,
    COD_DIS CHAR(5) NOT NULL REFERENCES DISTRITO
)
GO

CREATE TABLE CLIENTE(
    COD_CLI CHAR(5) NOT NULL PRIMARY KEY,
    RSO_CLI CHAR(30) NOT NULL,
    DIR_CLI VARCHAR(100) NOT NULL,
    TLF_CLI CHAR(9) NOT NULL,
    RUC_CLI CHAR(11) NULL,
    COD_DIS CHAR(5) NOT NULL REFERENCES DISTRITO,
    FEC_REG DATE NOT NULL,
    TIP_CLI VARCHAR(10) NOT NULL,
    CON_CLI VARCHAR(30) NOT NULL
)
GO

CREATE TABLE PROVEEDOR(
    COD_PRV CHAR(5) NOT NULL PRIMARY KEY,
    RSO_PRV VARCHAR(80) NOT NULL,
    DIR_PRV VARCHAR(100) NOT NULL,
    TEL_PRV CHAR(15) NULL,
    COD_DIS CHAR(5) NOT NULL REFERENCES DISTRITO,
    REP_PRV VARCHAR(80) NOT NULL
)
GO

CREATE TABLE FACTURA(
    NUM_FAC VARCHAR(12) NOT NULL PRIMARY KEY,
    FEC_FAC DATE NOT NULL,
    COD_CLI CHAR(5) NOT NULL REFERENCES CLIENTE,
    FEC_CAN DATE NOT NULL,
    EST_FAC VARCHAR(10) NOT NULL,
    COD_VEN CHAR(3) NOT NULL REFERENCES VENDEDOR,
    POR_IGV DECIMAL NOT NULL
)
GO

CREATE TABLE ORDEN_COMPRA(
    NUM_OCO CHAR(5) NOT NULL PRIMARY KEY,
    FEC_OCO DATE NOT NULL,
    COD_PRV CHAR(5) NOT NULL REFERENCES PROVEEDOR,
    FAT_OCO DATE NOT NULL,
    EST_OCO CHAR(1) NOT NULL
)
GO

CREATE TABLE PRODUCTO(
    COD_PRO CHAR(5) NOT NULL PRIMARY KEY,
    DES_PRO VARCHAR(50) NOT NULL,
    PRE_PRO MONEY NOT NULL,
    SAC_PRO INT NOT NULL,
    SMI_PRO INT NOT NULL,
	UNI_PRO VARCHAR(30) NOT NULL,
    LIN_PRO VARCHAR(30) NOT NULL,
    IMP_PRO VARCHAR(10) NOT NULL
)
GO

CREATE TABLE DETALLE_FACTURA(
    NUM_FAC VARCHAR(12) NOT NULL REFERENCES FACTURA,
    COD_PRO CHAR(5) NOT NULL REFERENCES PRODUCTO,
    CAN_VEN INT NOT NULL,
    PRE_VEN MONEY NOT NULL,
    PRIMARY KEY (NUM_FAC, COD_PRO)
)
GO

CREATE TABLE DETALLE_COMPRA(
    NUM_OCO CHAR(5) NOT NULL REFERENCES ORDEN_COMPRA,
    COD_PRO CHAR(5) NOT NULL REFERENCES PRODUCTO,
    CAN_DET INT NOT NULL,
    PRIMARY KEY (NUM_OCO, COD_PRO)
)
GO

CREATE TABLE ABASTECIMIENTO(
    COD_PRV CHAR(5) NOT NULL REFERENCES PROVEEDOR,
    COD_PRO CHAR(5) NOT NULL REFERENCES PRODUCTO,
    PRE_ABA MONEY NOT NULL,
    PRIMARY KEY (COD_PRV, COD_PRO)
)
GO
use BDVENTAS
go

INSERT INTO DISTRITO VALUES('D01', 'SURCO');
INSERT INTO DISTRITO VALUES('D02', 'JESUS MARIA');
INSERT INTO DISTRITO VALUES('D03', 'SAN ISIDRO');
INSERT INTO DISTRITO VALUES('D04', 'LA MOLINA');
INSERT INTO DISTRITO VALUES('D05', 'SAN MIGUEL');
INSERT INTO DISTRITO VALUES('D06', 'MIRAFLORES');
INSERT INTO DISTRITO VALUES('D07', 'BARRANCO');
INSERT INTO DISTRITO VALUES('D08', 'CHORRILLOS');
INSERT INTO DISTRITO VALUES('D09', 'SAN BORJA');
INSERT INTO DISTRITO VALUES('D10', 'LINCE');
INSERT INTO DISTRITO VALUES('D11', 'BREÑA');
INSERT INTO DISTRITO VALUES('D12', 'MAGDALENA');
INSERT INTO DISTRITO VALUES('D13', 'RIMAC');
INSERT INTO DISTRITO VALUES('D14', 'SURQUILLO');
INSERT INTO DISTRITO VALUES('D15', 'PUEBLO LIBRE');
INSERT INTO DISTRITO VALUES('D16', 'BELLAVISTA');
INSERT INTO DISTRITO VALUES('D17', 'CALLAO');
INSERT INTO DISTRITO VALUES('D18', 'SAN MARTIN DE PORRES');
INSERT INTO DISTRITO VALUES('D19', 'SANTA ANITA');
INSERT INTO DISTRITO VALUES('D20', 'LOS OLIVOS');
INSERT INTO DISTRITO VALUES('D21', 'INDEPENDENCIA');
INSERT INTO DISTRITO VALUES('D22', 'LIMA - CERCADO');
INSERT INTO DISTRITO VALUES('D23', 'SAN LUIS');
INSERT INTO DISTRITO VALUES('D24', 'EL AGUSTINO');
INSERT INTO DISTRITO VALUES('D25', 'SAN JUAN DE LURIGANCHO');
INSERT INTO DISTRITO VALUES('D26', 'ATE - VITARTE');
INSERT INTO DISTRITO VALUES('D27', 'SAN JUAN DE MIRAFLORES');
INSERT INTO DISTRITO VALUES('D28', 'CARMEN DE LA LEGUA');
INSERT INTO DISTRITO VALUES('D29', 'COMAS');
INSERT INTO DISTRITO VALUES('D30', 'VILLA MARIA DEL TRIUNFO');
INSERT INTO DISTRITO VALUES('D31', 'VILLA EL SALVADOR');
INSERT INTO DISTRITO VALUES('D32', 'LA PERLA');
INSERT INTO DISTRITO VALUES('D33', 'VENTANILLA');
INSERT INTO DISTRITO VALUES('D34', 'PUENTE PIEDRA');
INSERT INTO DISTRITO VALUES('D35', 'CARABAYLLO');
INSERT INTO DISTRITO VALUES('D36', 'SANTA MARIA');

INSERT INTO VENDEDOR VALUES('V01', 'JUANA', 'ALVA', 1000, CONVERT(DATE, '07/12/2003',103)), '1', 'D08');
INSERT INTO VENDEDOR VALUES('V02', 'JUAN', 'SOTO', 1200, CONVERT(DATE, '10/03/2002',103)), '2', 'D03');
INSERT INTO VENDEDOR VALUES('V03', 'CARLOS', 'AREVALO', 1500, CONVERT(DATE, '02/10/1992',103)), '2', 'D09');
INSERT INTO VENDEDOR VALUES('V04', 'CESAR', 'OJEDA', 850, CONVERT(DATE, '08/11/1999',103)), '1', 'D01');
INSERT INTO VENDEDOR VALUES('V05', 'JULIO', 'VEGA', 1500, CONVERT(DATE, '11/05/1997',103)), '1', 'D01');
INSERT INTO VENDEDOR VALUES('V06', 'ANA', 'ORTEGA', 1200, CONVERT(DATE, '11/05/1994',103)), '1', 'D05');
INSERT INTO VENDEDOR VALUES('V07', 'JOSE', 'PALACIOS', 2500, CONVERT(DATE, '12/02/1997',103)), '1', 'D02');
INSERT INTO VENDEDOR VALUES('V08', 'RUBEN', 'GOMEZ', 1450, CONVERT(DATE, '12/08/1996',103)), '2', 'D04');
INSERT INTO VENDEDOR VALUES('V09', 'PATRICIA', 'ARCE', 1800, CONVERT(DATE, '12/05/1997',103)), '2', 'D04');
INSERT INTO VENDEDOR VALUES('V10', 'RENATO', 'PEREZ', 1550, CONVERT(DATE, '11/08/1998',103)), '2', 'D01');

INSERT INTO CLIENTE VALUES('C001','FINSETH','AV. LOS VIÑEDOS 150','4343218','48632881','D05',CONVERT(DATE,'12/10/2008', 103),'1','ALICIA BARRETO');
INSERT INTO CLIENTE VALUES('C002','OBRI','AV. EMILIO CAVENECIA 225','4406335','57031642','D04',CONVERT(DATE,'02/01/2013', 103),'2','ALFONSO BELTRAN');
INSERT INTO CLIENTE VALUES('C003','SERVIEMSA','JR. COLLAGATE 522','75012403','NULL','D05',CONVERT(DATE,'06/03/2011', 103),'2','CHRISTIAN LAGUNA');
INSERT INTO CLIENTE VALUES('C004','ISSA','CALLE LOS ALIADORES 263','57725910','46702159','D01',CONVERT(DATE,'9/12/2012', 103), '1','LUIS APUMAYTA');
INSERT INTO CLIENTE VALUES('C005','MASS','AV. TOMAS MARSANO 880','4446771','88375942','D14',CONVERT(DATE,'10/01/2013', 103),'1','KATIA ARMEJO');
INSERT INTO CLIENTE VALUES('C006','BEKER','AV. LOS PROCERES 521','810322','54890124','D05',CONVERT(DATE,'7/05/2013', 103),'1','JUDITH ASE');
INSERT INTO CLIENTE VALUES('C007','FIDENZA','JR. EL ITIQUEL 282','5280934','16204799','D20',CONVERT(DATE,'10/02/2012', 103),'2','HECTOR VIVANCO');
INSERT INTO CLIENTE VALUES('C008','INTECH','AV. SAN LUIS 2619 5TO P.','22494937','34021824','D09',CONVERT(DATE,'01/07/2011', 103),'2','CARLOS VILLANUEVA');
INSERT INTO CLIENTE VALUES('C009','PROMINENT','JR. IQUIQUE 132','43231519','NULL','D11',CONVERT(DATE,'06/11/2010', 103),'1','JORGE VALDIVIA');
INSERT INTO CLIENTE VALUES('C010','LANDU','AV. NICOLAS DE A DIEZ 1453','567846','30405261','D06',CONVERT(DATE,'11/06/2013', 103),'2','RAQUEL ESPINOZA');
INSERT INTO CLIENTE VALUES('C011','LILAS','AV. EL POLO 1189','4598175','70345201','D26',CONVERT(DATE,'08/04/2012', 103),'1','ANGELICA UTVAS');
INSERT INTO CLIENTE VALUES('C012','SUCERTE','JR. GRITO DE HUAM 114','4264634','62014583','D05',CONVERT(DATE,'11/08/2011', 103),'1','KARINA VEGA');
INSERT INTO CLIENTE VALUES('C013','HAYASHI','JR. AYACUCHO 359','42847990','NULL','D22',CONVERT(DATE,'03/09/2013', 103),'2','ERNESTO UEHARA');
INSERT INTO CLIENTE VALUES('C014','KADIA','AV. SANTA CRUZ 1332 OF.201','4441281','22202195','D06',CONVERT(DATE,'10/05/2011', 103),'1','MIGUEL ARCE');
INSERT INTO CLIENTE VALUES('C015','MEBA','AV. ELMER FAUCETT 1638','4643217','50315942','D16',CONVERT(DATE,'06/11/2011', 103),'2','RICARDO GOMEZ');
INSERT INTO CLIENTE VALUES('C016','CARDELIR','JR. BARTOLOME HERRERA 451','2658853','26403158','D10',CONVERT(DATE,'5/04/2013', 103), '2','GIANCARLO BONIFAZ');
INSERT INTO CLIENTE VALUES('C017','PAYET','CALLE JUAN FANING 327','4779834','70594032','D07',CONVERT(DATE,'5/12/2013', 103), '1','PAOLA URIBE');
INSERT INTO CLIENTE VALUES('C018','DASIN','AV. SAENZ PEÑA 338 - B','4657574','35167562','D17',CONVERT(DATE,'12/03/2011', 103),'1','ANGELA BARRETO');
INSERT INTO CLIENTE VALUES('C019','COREFO','AV. CANADA 3894','4377499','57201691','D24',CONVERT(DATE,'01/03/2010', 103),'2','ROSALYN CORTEZ');
INSERT INTO CLIENTE VALUES('C020','CRAMER','JR. MARISCAL MILLER 1131','4719061','46631783','D02',CONVERT(DATE,'8/11/2010', 103),'1','CHRISTOPHER RAMOS');

INSERT INTO PROVEEDOR VALUES('PR01', 'FABER CASTELL', 'AV. ISABEL LA CATOLICA 1875', '433309', 'D13', 'CARLOS AGUIRRE');
INSERT INTO PROVEEDOR VALUES('PR02', 'ATLAS', 'AV. LIMA 471', '538092', 'D13', 'CESAR TORRES');
INSERT INTO PROVEEDOR VALUES('PR03', '3M', 'AV. VENEZUELA 3018', '290815', 'D16', 'OMAR INJOQUE');
INSERT INTO PROVEEDOR VALUES('PR04', 'DITO', 'AV. METROPOLITANA 376', 'NULL', 'D19', 'RAMON FLORES');
INSERT INTO PROVEEDOR VALUES('PR05', 'ACKER', 'CALLE LAS DUNAS 245', '478013', 'D27', 'JULIO ACUÑA');
INSERT INTO PROVEEDOR VALUES('PR06', 'DEDITEC', 'CALLE PICHINCHA 644', '566284', 'D11', 'JAVIER GONZALES');
INSERT INTO PROVEEDOR VALUES('PR07', 'OFFICETEC', 'CALLE LAS PERDICES 225 OF. 204', '421684', 'D03', 'CARLOS ROBLES');
INSERT INTO PROVEEDOR VALUES('PR08', 'INVICTA', 'AV. LOS FRUTALES 564', '436424', 'D27', 'ALBERTO RODRIGUEZ');
INSERT INTO PROVEEDOR VALUES('PR09', 'DIPROPOR', 'AV. DEL AIRE 901', '474204', 'D24', 'ROBERTO RONCEROS');
INSERT INTO PROVEEDOR VALUES('PR10', 'MIURA', 'AV. LA PAZ 257', '445971', 'D06', 'JORGE VASQUEZ');
INSERT INTO PROVEEDOR VALUES('PR11', 'PRAXIS', 'AV. JOSE GALVEZ 1820', '417894', 'D12', 'ERICKA ZEGARRA');
INSERT INTO PROVEEDOR VALUES('PR12', 'SUMIGRAF', 'AV. MANCO CAPAC 754', '332034', 'D13', 'KARINNA PAREDES');
INSERT INTO PROVEEDOR VALUES('PR13', 'LIMMSA', 'PROL. HUAYLAS 670', '253469', 'D08', 'LAURA ORTEGA');
INSERT INTO PROVEEDOR VALUES('PR14', 'VENINSA', 'AV. TEJADA 338', '287372', 'D07', 'MELISA RAMOS');
INSERT INTO PROVEEDOR VALUES('PR15', 'CROSLAND', 'AV. ARGENTINA 3206', '375472', 'D17', 'JUAN RAMIREZ');
INSERT INTO PROVEEDOR VALUES('PR16', 'PETRAMAS', 'CALLE JOAQUIN MADRID 1412', '395137', 'D02', 'VANESA QUINTANA');
INSERT INTO PROVEEDOR VALUES('PR17', 'REAWSE', 'AV. SANTA ROSA 480', 'NULL', 'D19', 'MARIA PEREZ');
INSERT INTO PROVEEDOR VALUES('PR18', 'EDUSA', 'AV. MORALES DUAREZ 1260', '452536', 'D29', 'PABLO MARTEL');
INSERT INTO PROVEEDOR VALUES('PR19', 'OTTMER', 'URB. PRO INDUSTRIAL MZ B6 LT 16', '536989', 'D18', 'ANGELEA RENDILLA');
INSERT INTO PROVEEDOR VALUES('PR20', 'BARI', 'AV. ARNALDO MARQUEZ 1219', 'NULL', 'D02', 'VANESA QUINTANA');

INSERT INTO FACTURA VALUES('FA001', CONVERT(DATE, '06-07-2013', 105), 'C001', CONVERT(DATE, '05-08-2013', 105), '2', 'V01', '0.19');
INSERT INTO FACTURA VALUES('FA002', CONVERT(DATE, '06-07-2013', 105), 'C019', CONVERT(DATE, '05-08-2013', 105), '3', 'V02', '0.19');
INSERT INTO FACTURA VALUES('FA003', CONVERT(DATE, '01-09-2013', 105), 'C003', CONVERT(DATE, '03-10-2013', 105), '2', 'V04', '0.19');
INSERT INTO FACTURA VALUES('FA004', CONVERT(DATE, '06-09-2013', 105), 'C016', CONVERT(DATE, '05-10-2013', 105), '2', 'V07', '0.19');
INSERT INTO FACTURA VALUES('FA005', CONVERT(DATE, '01-10-2013', 105), 'C015', CONVERT(DATE, '12-10-2013', 105), '2', 'V08', '0.19');
INSERT INTO FACTURA VALUES('FA006', CONVERT(DATE, '01-08-2013', 105), 'C009', CONVERT(DATE, '05-08-2013', 105), '3', 'V05', '0.19');
INSERT INTO FACTURA VALUES('FA007', CONVERT(DATE, '15-10-2013', 105), 'C019', CONVERT(DATE, '10-10-2013', 105), '1', 'V09', '0.19');
INSERT INTO FACTURA VALUES('FA008', CONVERT(DATE, '04-10-2013', 105), 'C012', CONVERT(DATE, '06-10-2013', 105), '2', 'V10', '0.19');
INSERT INTO FACTURA VALUES('FA009', CONVERT(DATE, '03-10-2013', 105), 'C008', CONVERT(DATE, '10-10-2013', 105), '2', 'V09', '0.19');
INSERT INTO FACTURA VALUES('FA010',CONVERT(DATE, '01-10-2013', 105),'C017',CONVERT(DATE, '06-10-2013', 105),'2','V02','0.19');
INSERT INTO FACTURA VALUES('FA011',CONVERT(DATE, '10-09-2013', 105),'C019',CONVERT(DATE, '10-09-2013', 105),'2','V05','0.19');
INSERT INTO FACTURA VALUES('FA012',CONVERT(DATE, '01-12-2013', 105),'C014',CONVERT(DATE, '01-12-2013', 105),'1','V04','0.19');
INSERT INTO FACTURA VALUES('FA013',CONVERT(DATE, '01-12-2013', 105),'C011',CONVERT(DATE, '01-12-2013', 105),'3','V08','0.19');
INSERT INTO FACTURA VALUES('FA014',CONVERT(DATE, '01-12-2012', 105),'C020',CONVERT(DATE, '01-12-2013', 105),'2','V09','0.19');
INSERT INTO FACTURA VALUES('FA015',CONVERT(DATE, '08-12-2012', 105),'C015',CONVERT(DATE, '06-01-2013', 105),'2','V07','0.19');
INSERT INTO FACTURA VALUES('FA016',CONVERT(DATE, '06-01-2013', 105),'C016',CONVERT(DATE, '09-01-2013', 105),'2','V05','0.19');
INSERT INTO FACTURA VALUES('FA017',CONVERT(DATE, '06-01-2013', 105),'C015',CONVERT(DATE, '06-01-2013', 105),'1','V06','0.19');
INSERT INTO FACTURA VALUES('FA018',CONVERT(DATE, '03-02-2013', 105),'C016',CONVERT(DATE, '04-02-2013', 105),'2','V10','0.19');
INSERT INTO FACTURA VALUES('FA019',CONVERT(DATE, '07-02-2013', 105),'C008',CONVERT(DATE, '01-03-2013', 105),'2','V03','0.19');
INSERT INTO FACTURA VALUES('FA020',CONVERT(DATE, '06-02-2013', 105),'C013',CONVERT(DATE, '10-03-2013', 105),'2','V02','0.19');

INSERT INTO ORDEN_COMPRA VALUES('OC001',CONVERT(DATE, '05-03-2013', 105),'PR08',CONVERT(DATE, '12-03-2013', 105),'1');
INSERT INTO ORDEN_COMPRA VALUES('OC002',CONVERT(DATE, '08-04-2013', 105),'PR16',CONVERT(DATE, '10-04-2013', 105),'1');
INSERT INTO ORDEN_COMPRA VALUES('OC003',CONVERT(DATE, '02-08-2013', 105),'PR10',CONVERT(DATE, '02-08-2013', 105),'3');
INSERT INTO ORDEN_COMPRA VALUES('OC004',CONVERT(DATE, '05-04-2013', 105),'PR01',CONVERT(DATE, '05-04-2013', 105),'3');
INSERT INTO ORDEN_COMPRA VALUES('OC005',CONVERT(DATE, '06-03-2013', 105),'PR07',CONVERT(DATE, '10-03-2013', 105),'1');
INSERT INTO ORDEN_COMPRA VALUES('OC006',CONVERT(DATE, '02-01-2013', 105),'PR19',CONVERT(DATE, '02-01-2013', 105),'1');
INSERT INTO ORDEN_COMPRA VALUES('OC007',CONVERT(DATE, '06-02-2013', 105),'PR20',CONVERT(DATE, '05-04-2013', 105),'3');
INSERT INTO ORDEN_COMPRA VALUES('OC008',CONVERT(DATE, '02-06-2013', 105),'PR04',CONVERT(DATE, '01-07-2013', 105),'1');
INSERT INTO ORDEN_COMPRA VALUES('OC009',CONVERT(DATE, '03-08-2013', 105),'PR11',CONVERT(DATE, '10-09-2013', 105),'1');
INSERT INTO ORDEN_COMPRA VALUES('OC010',CONVERT(DATE, '05-09-2013', 105),'PR01',CONVERT(DATE, '05-09-2013', 105),'1');
INSERT INTO ORDEN_COMPRA VALUES('OC011',CONVERT(DATE, '02-10-2013', 105),'PR03',CONVERT(DATE, '03-10-2013', 105),'1');
INSERT INTO ORDEN_COMPRA VALUES('OC012',CONVERT(DATE, '04-10-2013', 105),'PR14',CONVERT(DATE, '05-10-2013', 105),'3');
INSERT INTO ORDEN_COMPRA VALUES('OC013',CONVERT(DATE, '02-11-2013', 105),'PR05',CONVERT(DATE, '06-11-2013', 105),'1');
INSERT INTO ORDEN_COMPRA VALUES('OC014',CONVERT(DATE, '03-11-2013', 105),'PR19',CONVERT(DATE, '05-12-2013', 105),'1');
INSERT INTO ORDEN_COMPRA VALUES('OC015',CONVERT(DATE, '03-11-2013', 105),'PR18',CONVERT(DATE, '10-12-2013', 105),'1');
INSERT INTO ORDEN_COMPRA VALUES('OC016',CONVERT(DATE, '06-12-2013', 105),'PR06',CONVERT(DATE, '06-12-2013', 105),'3');
INSERT INTO ORDEN_COMPRA VALUES('OC017',CONVERT(DATE, '08-01-2013', 105),'PR09',CONVERT(DATE, '08-01-2013', 105),'1');
INSERT INTO ORDEN_COMPRA VALUES('OC018',CONVERT(DATE, '01-02-2013', 105),'PR20',CONVERT(DATE, '08-02-2013', 105),'1');
INSERT INTO ORDEN_COMPRA VALUES('OC019',CONVERT(DATE, '03-03-2013', 105),'PR11',CONVERT(DATE, '06-03-2013', 105),'1');
INSERT INTO ORDEN_COMPRA VALUES('OC020',CONVERT(DATE, '02-04-2013', 105),'PR05',CONVERT(DATE, '02-04-2013', 105),'2');

INSERT INTO PRODUCTO VALUES('P001','PAPEL BOND A-4',35,200,1500,'MLL','2','V');
INSERT INTO PRODUCTO VALUES('P002','PAPEL BOND OFICIO',35,50,1500,'MLL','2','F');
INSERT INTO PRODUCTO VALUES('P003','PAPEL BULKY ',10,498,1000,'MLL','2','V');
INSERT INTO PRODUCTO VALUES('P004','PAPEL PERIODICO',9,4285,1000,'MLL','2','F');
INSERT INTO PRODUCTO VALUES('P005','CARTUCHO TINTA NEGRA',40,50,30,'UNI','1','F');
INSERT INTO PRODUCTO VALUES('P006','CARTUCHO TINTA COLOR',45,58,35,'UNI','1','F');
INSERT INTO PRODUCTO VALUES('P007','PORTA DISKETTES',3.5,30,80,'UNI','1','V');
INSERT INTO PRODUCTO VALUES('P008','CAJA DE DISKETTES * 10',30,125,180,'UNI','1','F');
INSERT INTO PRODUCTO VALUES('P009','BORRADOR DE TINTA ',10,100,500,'DOC','3','V');
INSERT INTO PRODUCTO VALUES('P010','BORRADOR BLANCO ',8,2000,4000,'DOC','3','F');
INSERT INTO PRODUCTO VALUES('P011','TAJADOR METAL ',20,1128,300,'DOC','3','F');
INSERT INTO PRODUCTO VALUES('P012','TAJADOR PLASTICO',12,608,380,'DOC','3','F');
INSERT INTO PRODUCTO VALUES('P013','FOLDER MANILA OFICIO',20,200,150,'CIE','3','F');
INSERT INTO PRODUCTO VALUES('P014','FOLDER MANILA A-4',20,150,150,'CIE','3','V');
INSERT INTO PRODUCTO VALUES('P015','SOBRE MANILA OFICIO',15,300,130,'CIE','3','F');
INSERT INTO PRODUCTO VALUES('P016','SOBRE MANILA A-4',18,200,100,'CIE','3','F');
INSERT INTO PRODUCTO VALUES('P017','LAPICERO NEGRO',10,3000,1000,'DOC','3','F');
INSERT INTO PRODUCTO VALUES('P018','LAPICERO AZUL',10,2013,1500,'DOC','3','F');
INSERT INTO PRODUCTO VALUES('P019','LAPICERO ROJO',8,1000,1000,'DOC','3','V');
INSERT INTO PRODUCTO VALUES('P020','FOLDER PLASTICO A-4',50,380,1000,'CIE','3','F');
INSERT INTO PRODUCTO VALUES('P021','PROTECTOR DE PANTALLA',50,20,5,'UNI','1','F');

INSERT INTO DETALLE_FACTURA VALUES('FA001','P007','6','5');
INSERT INTO DETALLE_FACTURA VALUES('FA001','P011','25','25');
INSERT INTO DETALLE_FACTURA VALUES('FA001','P013','11','20');
INSERT INTO DETALLE_FACTURA VALUES('FA002','P004','8','10');
INSERT INTO DETALLE_FACTURA VALUES('FA003','P002','10','40');
INSERT INTO DETALLE_FACTURA VALUES('FA003','P011','6','20');
INSERT INTO DETALLE_FACTURA VALUES('FA003','P017','12','10');
INSERT INTO DETALLE_FACTURA VALUES('FA003','P019','3','10');
INSERT INTO DETALLE_FACTURA VALUES('FA004','P009','5','5');
INSERT INTO DETALLE_FACTURA VALUES('FA005','P003','20','8');
INSERT INTO DETALLE_FACTURA VALUES('FA005','P020','50','50');
INSERT INTO DETALLE_FACTURA VALUES('FA006','P003','1','35');
INSERT INTO DETALLE_FACTURA VALUES('FA006','P002','20','35');
INSERT INTO DETALLE_FACTURA VALUES('FA006','P009','15','10');
INSERT INTO DETALLE_FACTURA VALUES('FA007','P002','12','5');
INSERT INTO DETALLE_FACTURA VALUES('FA007','P004','8','12');
INSERT INTO DETALLE_FACTURA VALUES('FA008','P004','15','30');
INSERT INTO DETALLE_FACTURA VALUES('FA008','P002','50','30');
INSERT INTO DETALLE_FACTURA VALUES('FA009','P003','8','10');
INSERT INTO DETALLE_FACTURA VALUES('FA010','P002','3','10');
INSERT INTO DETALLE_FACTURA VALUES('FA011','P002','20','35');
INSERT INTO DETALLE_FACTURA VALUES('FA012','P002','13','35');
INSERT INTO DETALLE_FACTURA VALUES('FA012','P009','15','50');
INSERT INTO DETALLE_FACTURA VALUES('FA013','P003','5','12');
INSERT INTO DETALLE_FACTURA VALUES('FA013','P015','3','15');
INSERT INTO DETALLE_FACTURA VALUES('FA014','P009','10','8');
INSERT INTO DETALLE_FACTURA VALUES('FA015','P008','5','30');
INSERT INTO DETALLE_FACTURA VALUES('FA015','P016','3','18');
INSERT INTO DETALLE_FACTURA VALUES('FA016','P008','2','30');
INSERT INTO DETALLE_FACTURA VALUES('FA017','P002','12','40');
INSERT INTO DETALLE_FACTURA VALUES('FA017','P005','120','40');
INSERT INTO DETALLE_FACTURA VALUES('FA018','P005','6','40');
INSERT INTO DETALLE_FACTURA VALUES('FA018','P005','6','40');
INSERT INTO DETALLE_FACTURA VALUES('FA019','P003','6','10');
INSERT INTO DETALLE_FACTURA VALUES('FA019','P008','2','45');
INSERT INTO DETALLE_FACTURA VALUES('FA020','P009','120','10');
INSERT INTO DETALLE_FACTURA VALUES('FA020','P015','5','15');

INSERT INTO DETALLE_COMPRA VALUES('OC001','P006','100');
INSERT INTO DETALLE_COMPRA VALUES('OC001','P016','20');
INSERT INTO DETALLE_COMPRA VALUES('OC002','P003','200');
INSERT INTO DETALLE_COMPRA VALUES('OC002','P005','500');
INSERT INTO DETALLE_COMPRA VALUES('OC003','P005','50');
INSERT INTO DETALLE_COMPRA VALUES('OC004','P009','10');
INSERT INTO DETALLE_COMPRA VALUES('OC004','P013','50');
INSERT INTO DETALLE_COMPRA VALUES('OC005','P017','150');
INSERT INTO DETALLE_COMPRA VALUES('OC005','P008','10');
INSERT INTO DETALLE_COMPRA VALUES('OC008','P002','10');
INSERT INTO DETALLE_COMPRA VALUES('OC008','P012','100');
INSERT INTO DETALLE_COMPRA VALUES('OC009','P009','50');
INSERT INTO DETALLE_COMPRA VALUES('OC009','P011','50');
INSERT INTO DETALLE_COMPRA VALUES('OC010','P001','100');
INSERT INTO DETALLE_COMPRA VALUES('OC011','P008','5');
INSERT INTO DETALLE_COMPRA VALUES('OC011','P016','10');
INSERT INTO DETALLE_COMPRA VALUES('OC012','P007','50');
INSERT INTO DETALLE_COMPRA VALUES('OC012','P011','100');
INSERT INTO DETALLE_COMPRA VALUES('OC013','P013','50');
INSERT INTO DETALLE_COMPRA VALUES('OC014','P004','50');
INSERT INTO DETALLE_COMPRA VALUES('OC014','P008','5');
INSERT INTO DETALLE_COMPRA VALUES('OC014','P020','50');
INSERT INTO DETALLE_COMPRA VALUES('OC016','P015','100');
INSERT INTO DETALLE_COMPRA VALUES('OC017','P012','100');
INSERT INTO DETALLE_COMPRA VALUES('OC017','P014','100');
INSERT INTO DETALLE_COMPRA VALUES('OC019','P006','100');
INSERT INTO DETALLE_COMPRA VALUES('OC020','P005','500');
INSERT INTO DETALLE_COMPRA VALUES('OC020','P011','100');

INSERT INTO ABASTECIMIENTO VALUES('PR01','P003','8');
INSERT INTO ABASTECIMIENTO VALUES('PR01','P005','35');
INSERT INTO ABASTECIMIENTO VALUES('PR01','P007','3');
INSERT INTO ABASTECIMIENTO VALUES('PR01','P009','8');
INSERT INTO ABASTECIMIENTO VALUES('PR01','P011','18');
INSERT INTO ABASTECIMIENTO VALUES('PR02','P002','30');
INSERT INTO ABASTECIMIENTO VALUES('PR02','P007','3');
INSERT INTO ABASTECIMIENTO VALUES('PR03','P002','32');
INSERT INTO ABASTECIMIENTO VALUES('PR03','P004','7');
INSERT INTO ABASTECIMIENTO VALUES('PR04','P001','28');
INSERT INTO ABASTECIMIENTO VALUES('PR04','P006','40');
INSERT INTO ABASTECIMIENTO VALUES('PR05','P018','9');
INSERT INTO ABASTECIMIENTO VALUES('PR06','P009','7');
INSERT INTO ABASTECIMIENTO VALUES('PR06','P017','8');
INSERT INTO ABASTECIMIENTO VALUES('PR07','P016','15');
INSERT INTO ABASTECIMIENTO VALUES('PR07','P019','7');
INSERT INTO ABASTECIMIENTO VALUES('PR08','P006','42');
INSERT INTO ABASTECIMIENTO VALUES('PR08','P010','6');
INSERT INTO ABASTECIMIENTO VALUES('PR09','P002','30');
INSERT INTO ABASTECIMIENTO VALUES('PR09','P014','17');
INSERT INTO ABASTECIMIENTO VALUES('PR11','P001','27');
INSERT INTO ABASTECIMIENTO VALUES('PR11','P006','44');
INSERT INTO ABASTECIMIENTO VALUES('PR12','P002','33');
INSERT INTO ABASTECIMIENTO VALUES('PR12','P010','7');
INSERT INTO ABASTECIMIENTO VALUES('PR13','P005','35');
INSERT INTO ABASTECIMIENTO VALUES('PR14','P016','15');
INSERT INTO ABASTECIMIENTO VALUES('PR15','P026','45');
INSERT INTO ABASTECIMIENTO VALUES('PR16','P008','25');
INSERT INTO ABASTECIMIENTO VALUES('PR16','P012','9');
INSERT INTO ABASTECIMIENTO VALUES('PR16','P003','15');
INSERT INTO ABASTECIMIENTO VALUES('PR16','P008','28');
INSERT INTO ABASTECIMIENTO VALUES('PR19','P016','16');
INSERT INTO ABASTECIMIENTO VALUES('PR20','P012','10');
INSERT INTO ABASTECIMIENTO VALUES('PR20','P020','43');

```
<kbd>
  <img src="IMG/base.png" />
</kbd>
