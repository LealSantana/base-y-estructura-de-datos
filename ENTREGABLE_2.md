## ENTREGABLE NUMERO 2

# Creacion de la base de datos 


```
SET DATEFORMAT YMD
GO

IF DB_ID('BDventas') IS NOT NULL
    DROP DATABASE BDventas
GO

CREATE DATABASE BDventas
GO

USE BDVentas
GO

CREATE TABLE DISTRITO(
    COD_DIS CHAR(5) NOT NULL PRIMARY KEY,
    NOM_DIS VARCHAR(50) NOT NULL
)
GO

CREATE TABLE VENDEDOR(
    COD_VEN CHAR(3) NOT NULL PRIMARY KEY,
    NOM_VEN VARCHAR(20) NOT NULL,
    APE_VEN VARCHAR(20) NOT NULL,
    SUE_VEN MONEY NOT NULL,
    FTI_VEN DATE NOT NULL,
    TIP_VEN VARCHAR(10) NOT NULL,
    COD_DIS CHAR(5) NOT NULL REFERENCES DISTRITO
)
GO

CREATE TABLE CLIENTE(
    COD_CLI CHAR(5) NOT NULL PRIMARY KEY,
    RSO_CLI CHAR(30) NOT NULL,
    DIR_CLI VARCHAR(100) NOT NULL,
    TLF_CLI CHAR(9) NOT NULL,
    RUC_CLI CHAR(11) NULL,
    COD_DIS CHAR(5) NOT NULL REFERENCES DISTRITO,
    FEC_REG DATE NOT NULL,
    TIP_CLI VARCHAR(10) NOT NULL,
    CON_CLI VARCHAR(30) NOT NULL
)
GO

CREATE TABLE PROVEEDOR(
    COD_PRV CHAR(5) NOT NULL PRIMARY KEY,
    RSO_PRV VARCHAR(80) NOT NULL,
    DIR_PRV VARCHAR(100) NOT NULL,
    TEL_PRV CHAR(15) NULL,
    COD_DIS CHAR(5) NOT NULL REFERENCES DISTRITO,
    REP_PRV VARCHAR(80) NOT NULL
)
GO

CREATE TABLE FACTURA(
    NUM_FAC VARCHAR(12) NOT NULL PRIMARY KEY,
    FEC_FAC DATE NOT NULL,
    COD_CLI CHAR(5) NOT NULL REFERENCES CLIENTE,
    FEC_CAN DATE NOT NULL,
    EST_FAC VARCHAR(10) NOT NULL,
    COD_VEN CHAR(3) NOT NULL REFERENCES VENDEDOR,
    POR_IGV DECIMAL NOT NULL
)
GO

CREATE TABLE ORDEN_COMPRA(
    NUM_OCO CHAR(5) NOT NULL PRIMARY KEY,
    FEC_OCO DATE NOT NULL,
    COD_PRV CHAR(5) NOT NULL REFERENCES PROVEEDOR,
    FAT_OCO DATE NOT NULL,
    EST_OCO CHAR(1) NOT NULL
)
GO

CREATE TABLE PRODUCTO(
    COD_PRO CHAR(5) NOT NULL PRIMARY KEY,
    DES_PRO VARCHAR(50) NOT NULL,
    PRE_PRO MONEY NOT NULL,
    SAC_PRO INT NOT NULL,
    SMI_PRO INT NOT NULL,
	UNI_PRO VARCHAR(30) NOT NULL,
    LIN_PRO VARCHAR(30) NOT NULL,
    IMP_PRO VARCHAR(10) NOT NULL
)
GO

CREATE TABLE DETALLE_FACTURA(
    NUM_FAC VARCHAR(12) NOT NULL REFERENCES FACTURA,
    COD_PRO CHAR(5) NOT NULL REFERENCES PRODUCTO,
    CAN_VEN INT NOT NULL,
    PRE_VEN MONEY NOT NULL,
    PRIMARY KEY (NUM_FAC, COD_PRO)
)
GO

CREATE TABLE DETALLE_COMPRA(
    NUM_OCO CHAR(5) NOT NULL REFERENCES ORDEN_COMPRA,
    COD_PRO CHAR(5) NOT NULL REFERENCES PRODUCTO,
    CAN_DET INT NOT NULL,
    PRIMARY KEY (NUM_OCO, COD_PRO)
)
GO

CREATE TABLE ABASTECIMIENTO(
    COD_PRV CHAR(5) NOT NULL REFERENCES PROVEEDOR,
    COD_PRO CHAR(5) NOT NULL REFERENCES PRODUCTO,
    PRE_ABA MONEY NOT NULL,
    PRIMARY KEY (COD_PRV, COD_PRO)
)
GO
```
<kbd>
  <img src="IMG/ENTR2.jpg" />
</kbd>


# Consultas 

```
SELECT * FROM CLIENTE;
SELECT * FROM PROVEEDOR;
SELECT * FROM FACTURA;
SELECT * FROM ORDEN_COMPRA;
SELECT * FROM PRODUCTO;
SELECT * FROM DETALLE_FACTURA;
SELECT * FROM DETALLE_COMPRA;
SELECT * FROM VENDEDOR;
SELECT * FROM DISTRITO;
SELECT * FROM ABASTECIMIENTO;
```

# CONSULTAS EN SQL SERVER 

> Mostrar los registros de los clientes cuyos codigos de distrito sea D01, D14 O D16.

```
SELECT C.*
FROM CLIENTE C 
WHERE COD_DIS IN ('D01','D14','D16')
GO
```
>Mostrar los registros de los clientes cuyo nombre de la razon social inicie con las letra M.

```
SELECT C.*
FROM CLIENTE C 
WHERE RSO_CLI LIKE 'M%'
GO
```

# CONSULTAS AVANZADAS 
# Consulta para obtener el total de productos vendidos y el importe total por cada factura
```
SELECT F.NUM_FAC, F.FEC_FAC, COUNT(DF.COD_PRO) AS TotalProductos, SUM(DF.CAN_VEN * DF.PRE_VEN) AS ImporteTotal
FROM FACTURA F
JOIN DETALLE_FACTURA DF ON F.NUM_FAC = DF.NUM_FAC
GROUP BY F.NUM_FAC, F.FEC_FAC
ORDER BY ImporteTotal DESC;
```
#UPDATE 
> ACTUALIZAR DIRECCION DE CLIENTE 

```
UPDATE CLIENTE
SET DIR_CLI = 'av.gardenias'
WHERE COD_CLI = 'C002';
```

# ACTUALIZAR PRECIO PRODUCTO

```
UPDATE PRODUCTO
SET PRE_PRO = 5511
WHERE COD_PRO = 'P001';
```


#DELETE 
> Eliminar un producto de la tabla 'PRODUCTO'

```
DELETE FROM CLIENTE
WHERE COD_CLI = 'C155';
```

> Eliminar un producto de la tabla 'PRODUCTO'

```
DELETE FROM PRODUCTO
WHERE COD_PRO = 'PR104';
```

# Casos de uso GROUP BY 


> Mostrar el monto acumulado( precio x cantidad) de la facturas.

```
SELECT F.NUM_FAC AS FACTURA, SUM(DF.CAN_VEN * DF.CAN_VEN) AS ACUMULADO
FROM FACTURA F
JOIN DETALLE_FACTURA DF ON F.NUM_FAC = DF.NUM_FAC
GROUP BY F.NUM_FAC
```

> Mostrar el total de facturas registradas por vendedor

```
SELECT V.NOM_VEN + SPACE(1) + V.APE_VEN AS VENDEDOR,
       COUNT(*) AS [TOTAL DE FACTURAS]
FROM FACTURA F
JOIN VENDEDOR V ON V.COD_VEN = F.COD_VEN
GROUP BY V.NOM_VEN + SPACE(1) + V.APE_VEN
```



# VALIDACION DE VISTA

> listar las facturas cuyo subtotal se encuentre entre $30 y $100 

```
 select D.* 
 FROM V_DETALLEFACTURA D
 WHERE D.SUBTOTAL BETWEEN 30 AND 100
 ```

>listar las facturas del cliente COREFO

```
 SELECT D.*
 FROM V_DETALLEFACTURA D
 WHERE D.CLIENTE = 'COREFO'
```

> validacion de la vista

``` 
 IF OBJECT_ID ('V.MANTENIMIENTOPRODUCTO') IS NOT NULL
 DROP VIEW V_MANTENIMIENTOPRODUCTO 
 GO
 ```
>CREANDO LA VISTA

```
 CREATE VIEW V_MANTENIMIENTOPRODUCTO
 AS 
 SELECT * FROM PRODUCTO
 GO
 ```

>PRUEBA

```
 SELECT * FROM V_MANTENIMIENTOPRODUCTO
 GO
```

# VALIDACION DE LA VISTA 1 
```
 IF OBJECT_ID ('V_FACTURA_EST1') IS NOT NULL
 DROP VIEW V_FACTURAS_EST1
 GO
 ```
> CREANDO LA VISTA

```
 CREATE VIEW V_FACTURAS_EST1
 AS 
 SELECT * FROM FACTURA F WHERE F.EST_FAC=1
 GO
 ```

# VALIDACION DE LA VISTA 2

```

 IF OBJECT_ID('V_FACTURAS_EST2') IS NOT NULL
 DROP VIEW V_FACTURAS_EST2
 GO
 ```
> CREADO LA VISTA

```
 CREATE VIEW V_FACTURAS_EST2
 AS SELECT * FROM FACTURA F WHERE F.EST_FAC = 2
 GO
 ```

>VALIDACION DE LA VISTA 3

```
 IF OBJECT_ID('V_FACTURAS_EST3') IS NOT NULL 
 DROP VIEW V_FACTURAS_EST3
 GO 
 ```
>CREANDO  LA VISTA

```
 CREATE VIEW V_FACTURAS_EST3 
 AS
 SELECT * FROM FACTURA F WHERE F.EST_FAC=3
 GO 
```
>PRUEBA DE VISTA HORIZONTES:

```

 SELECT * FROM V_FACTURAS_EST1
  SELECT * FROM V_FACTURAS_EST2
   SELECT * FROM V_FACTURAS_EST3
```

# PROCEDIMIENTO DE ALMACENAD DEFINIDOS POR EL USUARIO

> procedimiento almacenado para realizar reporte:

```
IF OBJECT_ID('SP_LISTADOVENDEDOR') IS NOT NULL
DROP PROC SP_LISTADOVENDEDOR
GO

CREATE PROC SP_LISTADOVENDEDOR
AS
SELECT 
    V.COD_VEN AS CODIGO,
    V.NOM_VEN + SPACE(1) + V.APE_VEN AS VENDEDOR,
    V.SUE_VEN AS SUELDO,
    V.FTI_VEN AS [FECHA DE INICIO],
    D.NOM_DIS AS DISTRITO
FROM 
    VENDEDOR V
JOIN 
    DISTRITO D ON V.COD_DIS = D.COD_DIS
GO
```

>PRUEBA EJEC

```
EXEC SP_LISTADOVENDEDOR
GO
```
 
# PROCEDIMIENTO DE ALMACENADO QUE PERMITA MOSTRAR INFORMACION DE UN DETERMINADO PRODUCTO 

```
IF OBJECT_ID('SP_MUESTRAPRODUCTO')IS NOT NULL
DROP PROC SP_MUESTRAPRODUCTO
GO 
CREATE PROC SP_MUESTRAPRODUCTO (@COD CHAR(5))
AS
SELECT 
P.COD_PRO AS CODIGO,
P.DES_PRO AS DESCRIPCION,
P.PRE_PRO AS PRECIO,
P.SAC_PRO AS [STOCK ACTUAL],
P.SMI_PRO AS [STOCK MINIMO]
FROM PRODUCTO P
WHERE P.COD_PRO=@COD
GO
```
> PRUEBA de MOSTRAR PRODUCTO 

```
EXEC SP_MUESTRAPRODUCTO 'P001'
```

>procerdimiento de almacenado que permita listar facturas de un determinado año y  de un determinado mes

```
IF OBJECT_ID('SP_FACTURASxAÑOxMES') IS NOT NULL

DROP PROC SP_FACTURASxAÑOxMES
GO

CREATE PROC SP_FACTURASxAÑOxMES (@AÑO INT,@MES INT)
AS
SELECT 
    F.NUM_FAC AS NUMERO,
    F.FEC_FAC AS [FECHA FACTURADA],
    C.RSO_CLI AS CLIENTE,
    F.FEC_CAN AS [FECHA CANCELACION],
    F.EST_FAC AS ESTADO,
    V.NOM_VEN + SPACE(1)+V.APE_VEN AS VENDEDOR
FROM FACTURA F
JOIN CLIENTE C ON C.COD_CLI = F.COD_CLI
JOIN VENDEDOR V ON V.COD_VEN = F.COD_VEN
WHERE YEAR(F.FEC_FAC) = @AÑO AND MONTH(F.FEC_FAC) = @MES
GO
```

> PRUEBA:

```
EXEC SP_FACTURASxAÑOxMES 2013,9
GO
```

#TIGGER

> Creando el trigger


#TRIGGER 1

```
CREATE TRIGGER TX_VALIDA_DISTRITO
ON DISTRITO
FOR INSERT
AS
IF (SELECT COUNT(*) FROM INSERTED, DISTRITO
WHERE INSERTED.NOM_DIS=DISTRITO.NOM_DIS) >1
BEGIN
DECLARE @DISTRITO VARCHAR(30), @COD CHAR(5)
SELECT @DISTRITO=NOM_DIS FROM INSERTED
ROLLBACK
SELECT @COD=COD_DIS FROM DISTRITO
WHERE DISTRITO.NOM_DIS=@DISTRITO
PRINT 'NOMBRE DE DISTRITO YA REGISTRADO EN LA TABLA'
PRINT ''
PRINT 'EL DISTRITO '+@DISTRITO+
' SE ENCUENTRA REGISTRADO CON EL CODIGO: '+@COD
END
ELSE
PRINT 'DISTRITO REGISTRADO CORRECTAMENTE'
GO

```

> PRUEBA

```
INSERT INTO DISTRITO VALUES ('D38', 'SANTA MARIA')
GO
PRINT ' DISTRITO REGISTRADO CORRECTAMENTE '
GO
```

# TRIGGETR 2
> Creando el trigger

```
CREATE TRIGGER TX_ELIMINACLIENTE
ON CLIENTE
INSTEAD OF DELETE
AS
IF EXISTS(SELECT * FROM FACTURA WHERE FACTURA.COD_CLI=(SELECT DELETED.COD_CLI
FROM DELETED))

BEGIN 
ROLLBACK TRANSACTION
PRINT 'EL CLIENTE TIENE FACTURA REGISTRADAS, NO PUEDE ELIMINARSE'
END
ELSE
PRINT 'CLIENTE ELIMINADO CORRECTAMENTE'
GO
```
>EJEMPLO 

```
DELETE CLIENTE WHERE COD_CLI='COO1'
```